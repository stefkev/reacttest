# FATUnicorn Front End Engineer Test
This is a coding proficiency test. You must be able to perform these 3 basic tasks on a day to day basis at FATUnicorn.

You will earn kudos for good documentation, error handling and appropriate use of third party and built in frameworks and libraries.

Your final commit must work in the latest versions of Chrome, Safari, IE and Microsoft Edge.

This test should take no longer than 2 hours to complete.

## Instructions
* Sign up to Parse.com (it’s free!)
* Fork this project
* Complete the test
* Commit and push to **your** repo
* Make a pull request
* Email **[jobs@fatunicorn.com](mailto:jobs@fatunicorn.com)** with a link to your pull request

## Frameworks, Tools & Libraries
* [ReactJS](https://facebook.github.io/react/)
* [ReactRouter](https://github.com/rackt/react-router)
* [Parse.com](https://www.parse.com/docs/js/guide)
* [WebPack](http://webpack.github.io/)
* [Yeoman](https://github.com/newtriks/generator-react-webpack)

# Tests
## Bootstrapping
* Bootstrap your react project using [WebPack and Yeoman](https://github.com/newtriks/generator-react-webpack)
* Install [React Router](https://github.com/rackt/react-router) as a dependancy
* Install [Parse](https://www.parse.com/docs/js/guide) and [Parse React](https://github.com/ParsePlatform/ParseReact) as a dependancy
* Commit and push your code for this test and tag it as **bootstrapping**

## Routes
* Create a routes for **Messages** and **New Message**
* Create React components for **Messages** and **New Message**
* Add Links from **Messages** to **New Message** and **New Messages** to **Messages** in the respective component render methods
* Commit and push your code for this test and tag it as **routes**

## Data
* Create a form in the **New Messages** component with the following fields.
	* Subject
	* Body
* Store the value of Subject and Body inside a Parse object called **Messages** and save it when the form is submitted
* Redirect to the **Messages** route if the save is successful
* List all of the messages in the **Messages** component
* Commit and push your code for this test and tag it as **data**